class Worm {
  float x0; //tail X coordinate
  float y0; //tail Y coordinate
  float x1; //head X coordinate
  float y1; //head Y coordinate

  int len; // length of the worm
  float alpha; // angle between the given worm and the x axis.

  float mass;
  float amplitude;
  float Pm;
  float speed;
  //float viscosityFactor;

  Worm(float xBegin, float yBegin, float massBegin, int wormLen, float amp) {
    x0 = xBegin;
    y0 = yBegin;
    x1 = xBegin;
    y1 = yBegin;
    mass = massBegin;
    len = wormLen;
    amplitude = amp;
    Pm = ((viscosityFactor) * 10)/ mass;
    speed = amplitude * Pm;
  }

  void update() {

    if  (/*y1 > 50 && */ speed != 0) {

      if (x1 >= 0 && x1 < width) {
        alpha = random(0, 180); 
        /*this allows the new x1 coordinate to be positive or negative 
         since cos(angle) is positive in the first quadrant and negative in the second quadrant. */
      } else if (x1 < 0) {
        alpha = random(0, 90);
        /*this allows the new x1 coordinate to only be positive
         since cos(angle) is positive if angle is in the first quadrant. */
      } else alpha = random(90, 180);
      /*this allows the new x1 coordinate to only be negative
       since cos(angle) is negative if angle is in the second quadrant. */

      //println(Pm);


      //rotate the worm
      y1 = y0 - len*(sin(radians(alpha))); 
      x1 = x0 + len*(cos(radians(alpha)));
      
      //translate the worm in a given direction
      x0 += speed*cos(radians(alpha));
      y0 -= speed*sin(radians(alpha));
      x1 += speed*cos(radians(alpha));
      y1 -= speed*sin(radians(alpha));
    }
  }
  void display() {    
    stroke(255, 235, 153);
    strokeWeight(mass/20.0);

    float angle = 0.0;
    float increment = (4*PI)/len; //increments angle by a constant = 2*pi/len

      float x1; 
    float y1; 
    float x2; 
    float y2;
    float prev_x2 = x0; 
    float prev_y2 = y0;

    for (int i=0; i < len; i++) {
      x1 = i;
      y1 = (sin(angle))*amplitude;

      //The computation below makes use of the 2x2 rotation matrix: [cos(angle)  -sin(angle)]
      //                                                            [sin(angle)   cos(angle)]
      // to rotate a certain point (x1, y1) by a certain angle about the origin.

      x2 = x1*cos(radians(alpha)) - y1*sin(radians(alpha));
      y2 = x1*sin(radians(alpha)) + y1*cos(radians(alpha));    
      //This makes sure that that the sine wave starts at (x0, y0) i.e. the tail point of the worm.
      x2 += x0;
      y2 += y0; 
      line(prev_x2, prev_y2, x2, y2);

      prev_x2 = x2;
      prev_y2 = y2;
      angle = angle + increment;
    }
  }
}    

//Declare the object worm
public static int nWorms = 50;
Worm[] worms = new Worm[nWorms];
public static float viscosityFactor = 1;
int i;
int frequencyForNewWorms = 0;

int newWorms = 5;

void setup() {
  size(640, 640);
  smooth();
  frameRate(05);
  // Create object worm and pass parameters
  for (i=0; i < nWorms; i++) {
    float mass = random(5, 15);
    //println(mass);
    Worm w = new Worm(random(0, width), (0.75)*height, mass, 20, random(0.1, 2.1));
    worms[i] = w;
  }
}

float calcDeter(float a, float b, float c, float d) {
  return (a*d - b*c);
}

boolean findAngle(float x0, float x1, float y0, float y1, float x2, float x3, float y2, float y3) {
float m1 = (y1 - y0) / (x1 - x0);
float m2 = (y3 - y2) / (x3 - x2); 
float angle = degrees(atan(m1) - atan(m2)); 
/* two vectors go in the same general direction if their dot product is greater than 0. 
These are not vectors, but checking to see if cos(angle) is positive yields the same result */
float least_angle = 90;
if(cos(radians(angle)) >= 0 && abs(angle) <= least_angle)
  return true;
else
  return false;
}

boolean checkIntersect(float xI, float yI, float x0, float y0, int len, float x1) {
  //return (dist(xI, yI, x0, y0) <= len);
  return (min(x0, x1) <= xI && xI <= max(x0, x1));
}

void draw() {
  background(0);

  if (frequencyForNewWorms == 10) {
    for (int n = nWorms; n < nWorms+newWorms; n++) {
      float mass = random(10, 30);
      //println(mass);
      Worm x = new Worm(random(0, width), (0.75)*height, mass, 20, random(0.1, 2.1));
      worms = (Worm[])append(worms, x);
    }

    frequencyForNewWorms = 0;
    nWorms += newWorms;
  }  


  frequencyForNewWorms += 1;

  //The code below  sets the speed of a random worm to 0.
  int randWorm = (int) random(nWorms);
  while (worms[randWorm] == null) {
    randWorm = (int) random(nWorms);
  }
  (worms[randWorm]).speed = 0;
  


  for (int l = 0; l < nWorms; l++) {

    if ((worms[l]) != null && (worms[l]).mass >= 80 && (worms[l]).speed != 0) {
      float rand = random(0, 1);  

      if (rand <= 0.65) {
        //worms split up if they're too heavy..
        println("Hey");
        (worms[l]).mass = (worms[l]).mass / 2.0;
        float sx0 = (worms[l]).x0 + 30;
        //create a new split up worm 30 units to the right of the heavy worm
        Worm s = new Worm(sx0, (worms[l]).y0, ((worms[l]).mass), 20, random(0.1, 2.1));
        s.x1 = (worms[l]).x1 + 30;
        worms = (Worm[])append(worms, s);
        nWorms += 1;

        (worms[l]).x0 = (worms[l]).x0 - 30;
        (worms[l]).x1 = (worms[l]).x1 - 30;
        (worms[l]).speed = (worms[l]).speed * 2;
      } else if (rand > 0.65 && (worms[l]).mass >= 80) {
        //worms stop moving if they're too heavy...
        println("Hi");
        (worms[l]).speed = 0; 
      } 
    }
  }

  for (int i = 0; i < nWorms; i++) {    
    if (worms[i] != null) {
      Worm w = (Worm) worms[i];
      w.update();
      //w.display();
    }
  }

  for (int j = 0; j < nWorms; j++) {
    if (worms[j] != null) { 
      Worm g = worms[j];
      float m1 = tan(radians(g.alpha)); //slope of given worm

      for (int k = j; k < nWorms; k++) {
        if (worms[k] != null) {
          Worm o = worms[k];
          float m2 = tan(radians(o.alpha)); 

          if (m1 != m2) {
            float dx = calcDeter(g.y0 - (m1*g.x0), 1, o.y0 - (m2*o.x0), 1);
            float dy = calcDeter(-m1, g.y1 - (m1*g.x1), -m2, o.y1 - (m2*o.x1));
            float d = calcDeter(-m1, 1, -m2, 1);

            float xI = dx/d; //x coordinate of point of intersection of the two worms (using Cramer's rule)
            float yI = dy/d;//y coordinate of point of intersection of the two worms (using Cramer's rule)

            if (g.speed != 0 && o.speed != 0 && 
              checkIntersect(xI, yI, g.x0, g.y0, g.len, g.x1) && checkIntersect(xI, yI, o.x0, o.y0, o.len, o.x1) && 
              findAngle(g.x0, g.x1, g.y0, g.y1, o.x0, o.x1, o.y0, o.y1)  == true) {
              //worms collide                 (worms[k]).nWormColl += 1 //+ (worms[j]).nWormColl;
              (worms[k]).mass += g.mass;
              worms[j] = null; 
              break; //is this the right place to have the break statement?
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < nWorms; i++) {    
    if (worms[i] != null) {
      Worm w = (Worm) worms[i];
      //w.update();
      w.display();
    }
  }
}

